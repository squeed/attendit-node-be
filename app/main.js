const mongoose = require('mongoose');
const WebSocket = require('ws');

const sendSocket = new WebSocket.Server({ port: 8080 });
const receiveSocket = new WebSocket.Server({ port: 8081 });

var mongoConnectionString = "mongodb://localhost:27017/dev"
if (process.env.MONGO_CONNECTION_STRING) {
  mongoConnectionString = process.env.MONGO_CONNECTION_STRING;
}

mongoose.connect(mongoConnectionString, {useNewUrlParser: true});
const CatModel = mongoose.model('Cat', { name: String, value: String });

sendSocket.on('connection', (ws) => {
  ws.on('message', (message) => {
    var json = JSON.parse(message);
    const kitty = new CatModel({ name: json.name, value: json.value });
    kitty.save().then(() => console.log('Saved: ' + message));
  });
  ws.send('Connected sendSocket');
});
  
receiveSocket.on('connection', (ws) => {
  CatModel.watch().on('change', data => {
    console.log(new Date(), data)
    ws.send(JSON.stringify(data.fullDocument));
  });
  ws.send('Connected receiveSocket');
});    