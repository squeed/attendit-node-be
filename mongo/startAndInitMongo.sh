#!/bin/bash

/usr/local/bin/docker-entrypoint.sh --replSet "rs" &
MONGOPID=$!
sleep 10
mongo --eval "rs.initiate()"
wait $MONGOPID